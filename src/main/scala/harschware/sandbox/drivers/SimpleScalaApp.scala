package harschware.sandbox.drivers

import java.io.File
import com.bizo.util.args4j.OptionsHelper.optionsOrExit
import com.bizo.util.args4j.OptionsWithHelp


/**
  *
  */
object SimpleScalaApp {
  def main(args: Array[String]) {
    val options = optionsOrExit(args, new MyOptions)
    val portInfoFile = options.arg1.getAbsolutePath

    System.out.println("arg1="+portInfoFile);
  } // end function


} // end object

class MyOptions extends OptionsWithHelp {
  @org.kohsuke.args4j.Option(name = "--arg1", usage = "some path", required = true)
  var arg1: File = new File("t")
}  // end class
