package harschware.sandbox.utils

import org.junit._
import org.junit.Assert._
import org.slf4j.LoggerFactory

@Test
class ScanPortTest {
  var LOGGER = LoggerFactory
    .getLogger(classOf[ScanPortTest]);

  @Test
  def testLocalHost() = {
    val localhost = "localhost";
    assertEquals("localhost", localhost)
  } // end test

} // end test class


